<?php
namespace keithe\FileUploader\Tests;

use keithe\FileUploader\FileUploader;

class ImageUploader extends FileUploader{

	protected $allowedMimeTypes = [
		'image/jpg',
		'image/png',
		'image/gif'
	];

}